﻿1
00:00:03,320 --> 00:00:05,820
Let's talk about the optical fiber deployment.

2
00:00:07,520 --> 00:00:11,680
In France, the article L1425-1 from the code of territorial collectivities tells that...

3
00:00:11,680 --> 00:00:14,680
"Networks set up by the collectivity must be accessible

4
00:00:14,680 --> 00:00:16,810
to <b>every</b> operator, without discrimination.<b></b>

5
00:00:17,310 --> 00:00:19,300
Everyone knows the very big operators,

6
00:00:19,300 --> 00:00:21,650
but there are hundreds of others, of any size.

7
00:00:22,570 --> 00:00:25,770
The tiny operators have problems to access these fiber networks.

8
00:00:26,860 --> 00:00:28,540
In cities and areas of semi density :

9
00:00:28,540 --> 00:00:30,080
the network is funded by the operators.

10
00:00:30,280 --> 00:00:32,550
But for all that remains of unprofitable areas

11
00:00:32,550 --> 00:00:33,960
− the majority of the territory −

12
00:00:33,960 --> 00:00:35,710
the network is financed by collectivities.

13
00:00:36,220 --> 00:00:38,190
That part of the network, paid by all of us,

14
00:00:38,190 --> 00:00:40,000
should be accessible to tiny operators.

15
00:00:42,330 --> 00:00:43,710
Operators that deploy these networks

16
00:00:43,720 --> 00:00:45,290
− private organisations

17
00:00:45,290 --> 00:00:47,180
but funded in this case by public money −

18
00:00:47,180 --> 00:00:48,240
strongly keep the access down.

19
00:00:49,820 --> 00:00:52,870
The Federation of non-profit Internet Service Providers

20
00:00:52,870 --> 00:00:54,270
− or shortly FFDN −

21
00:00:54,270 --> 00:00:57,980
has set up a barometer gauging the access to these public initiative networks

22
00:00:57,980 --> 00:01:00,620
and their compatibility with non-profit operators.

23
00:01:11,920 --> 00:01:13,970
There are many incompatibilities :

24
00:01:14,380 --> 00:01:17,080
Grey areas are the ones where we couldn't get any information

25
00:01:17,080 --> 00:01:19,180
whereas the official documentation is normally public.

26
00:01:19,860 --> 00:01:23,390
Black areas are the ones where there is no offer for tiny operators.

27
00:01:23,860 --> 00:01:27,140
Red areas are the ones where there is an offer, but unacceptable.

28
00:01:27,520 --> 00:01:29,410
Orange areas are the ones where there is an offer

29
00:01:29,410 --> 00:01:31,260
but pretty poor, or problematic.

30
00:01:31,630 --> 00:01:35,470
Yellow areas are the ones where there is an offer, not perfect , but without any major difficulty.

31
00:01:35,660 --> 00:01:38,890
Finally, green is for the only county where there is a reasonable offer.

32
00:01:38,890 --> 00:01:41,530
Knowing that the infos about it were only unlocked

33
00:01:41,530 --> 00:01:42,800
after we published the barometer.

34
00:01:43,380 --> 00:01:44,770
In conclusion,

35
00:01:44,770 --> 00:01:46,630
the vast majority of Public Initiative Networks

36
00:01:46,630 --> 00:01:48,210
are completely opaque or inexploitable.

37
00:01:51,670 --> 00:01:53,690
In the dynamic hope for a normal situation

38
00:01:53,690 --> 00:01:55,850
when public fiber networks would be finally accessible to

39
00:01:55,850 --> 00:01:57,470
every operator without discrimination;

40
00:01:57,900 --> 00:02:00,440
we can congratulate ourselves for this result in the Ain,

41
00:02:00,440 --> 00:02:03,960
and also for the signing of a contract by a tiny ISP member of the FFDN

42
00:02:03,960 --> 00:02:05,210
in one of the yellow areas.

